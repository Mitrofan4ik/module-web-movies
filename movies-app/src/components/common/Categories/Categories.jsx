import React from "react";
import { Tag } from "antd";

function CategoriesComponent({item, title, onChangeHandler, checkedItem, selectedFilter}) {
  return (
    <Tag.CheckableTag
      checked={selectedFilter.indexOf(checkedItem) > -1}
      onChange={(checked) => onChangeHandler(item, checked)}
    >
      {title}
    </Tag.CheckableTag>
  );
}

export default CategoriesComponent;
