import { createSlice, createAsyncThunk, createAction } from '@reduxjs/toolkit';
import { getLanguages } from '../../api'

export const fetchLanguages = createAsyncThunk('languages/fetchLanguages', async () => {
    const response = await getLanguages();
    return response
})

export const addSelectedLangs = createAction('languages/addSelectedLangs');

export const languagesSlice = createSlice({
    name: 'languages',
    initialState: {
        langs: [],
        selectedLangs: [],
        loaded: false,
        loading: false,
        errors: null,
    },
    reducers: {
        getAllLanguages: (state) => {
            console.log(state, 'state')
        },
    },
    extraReducers: builder => {
        builder
        .addCase(fetchLanguages.pending, (state, action) => {
            state.loading = true;
            state.loaded = false;
            state.error = null;
        })
        .addCase(fetchLanguages.fulfilled, (state, action) => {
            state.langs = action.payload;
            state.loaded = true;
            state.loading = false;
            state.error = null;
        })
        .addCase(fetchLanguages.rejected, (state, action) => {
            state.error = action.errors;
            state.loaded = true;
            state.loading = false;
        })
        .addCase(addSelectedLangs, (state, action) => {
            state.selectedLangs = action.payload;
        })
    }
})

export const { getAllLanguages } = languagesSlice.actions;

export default languagesSlice.reducer;