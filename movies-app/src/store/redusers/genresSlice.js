import { createSlice, createAsyncThunk, createAction } from '@reduxjs/toolkit';
import { getGenres } from '../../api'

export const fetchGenres = createAsyncThunk('movies/fetchGenres', async () => {
    const response = await getGenres();
    return response
})

export const addSelectedGenres = createAction('movie/addSelectedGenres');

export const genresSlice = createSlice({
    name: 'genres',
    initialState: {
        genres: [],
        selectedGenres: [],
        loaded: false,
        loading: false,
        errors: null,
    },
    reducers: {
        getAllGenres: (state) => {
            console.log(state, 'state')
        },
    },
    extraReducers: builder => {
        builder
            .addCase(fetchGenres.pending, (state, action) => {
                state.loading = true;
                state.loaded = false;
                state.error = null;
            })
            .addCase(fetchGenres.fulfilled, (state, action) => {
                state.genres = action.payload.genres;
                state.loaded = true;
                state.loading = false;
                state.error = null;
            })
            .addCase(fetchGenres.rejected, (state, action) => {
                state.error = action.errors;
                state.loaded = true;
                state.loading = false;
            })
            .addCase(addSelectedGenres, (state, action) => {
                state.selectedGenres = action.payload;
            })
    }
})

export const { getAllGenres } = genresSlice.actions;

export default genresSlice.reducer;