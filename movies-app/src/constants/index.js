export const FIELD_REQUERED = 'Field is required';
export const INVALID_FORMAT = 'Invalid format';
export const INVALID_PASSWORD_FORMAT = 'Password must be at least 6 characters';
export const INVALID_PASSWORD_MATCH_FORMAT = 'Passwords must match';

export const STATUS_ERROR = 'error';

export const ALL_MOVIES = 'All Movies';
export const FAVORITES = 'Favorites';

export const SEARCH = 'Search';
export const TYPE_SMTH = 'Type smth...';

export const FILTER_BY_GANRES = 'Filter by ganres';
export const FILTER_BY_LANGUAGE = 'Filter by Language';

export const ERROR_404 = 'Error 404';

export const LOG_OUT = 'Log out';
export const LOG_IN = 'Log in';
export const SIGN_UP = 'Sign up';
