import { createSlice, createAsyncThunk, createAction } from '@reduxjs/toolkit';
import { getMovies, getSearchMovies, getFilteredMovies } from '../../api'

export const fetchMovies = createAsyncThunk('movies/fetchMovies', async (page) => {
    const response = await getMovies(page);
    return response
})

export const fetchSearchMovies = createAsyncThunk('movies/fetchSearchMovies', async ({ searchKey, page }) => {
    const response = await getSearchMovies(searchKey, page);
    return response
})

export const fetchFilteredMovies = createAsyncThunk('movies/fetchFilteredMovies', async ({ genresId, lang, page }) => {
    const response = await getFilteredMovies(genresId, lang, page);
    return response
})

export const addToFavoritesMovies = createAction('movies/favorite', (movieId) => {
    return {
        payload: {
            id: movieId,
        },
    }
});

export const changePage = createAction('movie/page');
export const searchValue = createAction('movie/searchValue');

export const moviesSlice = createSlice({
    name: 'movies',
    initialState: {
        movies: [],
        totalPages: 0,
        page: 1,
        loaded: false,
        loading: false,
        errors: null,
        search: '',
    },
    reducers: {
        getAllMovies: (state) => {
            console.log(state, 'state')
        },
    },
    extraReducers: builder => {
        builder
            .addCase(fetchMovies.pending, (state, action) => {
                state.loading = true;
                state.loaded = false;
                state.error = null;
            })
            .addCase(fetchMovies.fulfilled, (state, action) => {
                state.movies = action.payload.results;
                state.totalPages = action.payload.total_pages;
                state.loaded = true;
                state.loading = false;
                state.error = null;
            })
            .addCase(fetchMovies.rejected, (state, action) => {
                state.error = action.errors;
                state.loaded = true;
                state.loading = false;
            })
            .addCase(addToFavoritesMovies, (state, action) => {
                let element = state.movies.find(movie => action.payload.id === movie.id)
                element.favorite = !element.favorite
            })
            .addCase(changePage, (state, action) => {
                state.page = action.payload;
            })
            .addCase(fetchSearchMovies.pending, (state, action) => {
                state.loading = true;
                state.loaded = false;
                state.error = null;
            })
            .addCase(fetchSearchMovies.fulfilled, (state, action) => {
                state.movies = action.payload.results;
                state.totalPages = action.payload.total_pages;
                state.loaded = true;
                state.loading = false;
                state.error = null;
            })
            .addCase(fetchSearchMovies.rejected, (state, action) => {
                state.error = action.errors;
                state.loaded = true;
                state.loading = false;
            })
            .addCase(searchValue, (state, action) => {
                state.search = action.payload;
            })
            .addCase(fetchFilteredMovies.pending, (state, action) => {
                state.loading = true;
                state.loaded = false;
                state.error = null;
            })
            .addCase(fetchFilteredMovies.fulfilled, (state, action) => {
                state.movies = action.payload.results;
                state.totalPages = action.payload.total_pages;
                state.loaded = true;
                state.loading = false;
                state.error = null;
            })
            .addCase(fetchFilteredMovies.rejected, (state, action) => {
                state.error = action.errors;
                state.loaded = true;
                state.loading = false;
            })
    },
})

export const { getAllMovies } = moviesSlice.actions;

export default moviesSlice.reducer;
