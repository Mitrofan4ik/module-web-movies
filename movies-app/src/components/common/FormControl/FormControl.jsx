import React from "react";
import { Input } from "antd";

function FormControl({
  type,
  placeholder,
  onChange,
  disabled = false,
  status,
  value
}) {
  return (
    <Input
      type={type}
      placeholder={placeholder}
      onChange={onChange}
      disabled={disabled}
      status={status}
      value={value}
    />
  );
}

export default FormControl;
