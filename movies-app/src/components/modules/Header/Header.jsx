import React from "react";
import { useNavigate } from "react-router-dom";

import { Col, Row } from "antd";
import ButtonComponent from "../../common/Button/Button";
import MenuComponent from "../../common/Menu/Menu";

function Header() {
  const navigate = useNavigate();
  const clearLocalStorage = () => {
    window.localStorage.clear();
    navigate("/login");
  };
  return (
    <Row gutter={16}>
      <Col span={20}>
        <MenuComponent />
      </Col>
      <Col span={4}>
        <ButtonComponent buttonText={"Log out"} onClickHandler={clearLocalStorage} />
      </Col>
    </Row>
  );
}

export default Header;
