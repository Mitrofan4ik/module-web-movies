import React from "react";
import { useDispatch } from "react-redux";
import { HeartTwoTone } from "@ant-design/icons";
import { Card } from "antd";
import { addToFavoritesMovies } from "../../../store/redusers/movieSlice";

function MovieCard({ imgSrc, title, description, alt, id, isFavorite }) {
  const dispatch = useDispatch();

  function statusCurrentMovie() {
    dispatch(addToFavoritesMovies(id));
  }

  return (
    <Card
      id={id}
      cover={
        !imgSrc ? (
          <img src="./empty.png" alt={alt}></img>
        ) : (
          <img alt={alt} src={`https://image.tmdb.org/t/p/original${imgSrc}`} />
        )
      }
      style={{ marginBottom: "16px" }}
      actions={[
        <HeartTwoTone
          twoToneColor={!isFavorite ? "#1890ff" : "#eb2f96"}
          onClick={statusCurrentMovie}
        />,
      ]}
    >
      <Card.Meta
        title={title}
        description={
          description.length > 50
            ? description.substring(0, 45) + "..."
            : description
        }
      />
    </Card>
  );
}

export default MovieCard;
