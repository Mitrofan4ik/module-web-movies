import React from "react";
import "./NotFound.scss";
import {ERROR_404} from '../../constants';

function NotFound() {
  return (
    <div className="error-page">
      <h1 className="error-title">{ERROR_404}</h1>
    </div>
  );
}

export default NotFound;
