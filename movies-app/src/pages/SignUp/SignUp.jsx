import React, { useCallback } from "react";
import { useNavigate } from "react-router-dom";
import FormControl from "../../components/common/FormControl/FormControl";
import ButtonComponent from "../../components/common/Button/Button";
import { Col, Row, Space } from "antd";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Radio } from "antd";
import RadioComponent from "../../components/common/RadioControl/RadioControl";
import DateComponent from "../../components/common/DateControl/DateControl";

import * as Yup from "yup";

import {
  FIELD_REQUERED,
  INVALID_FORMAT,
  STATUS_ERROR,
  INVALID_PASSWORD_FORMAT,
  INVALID_PASSWORD_MATCH_FORMAT,
  SIGN_UP
} from "../../constants";

const validationSchema = Yup.object().shape({
  firstName: Yup.string()
    .matches(/^[a-zA-z!]+$/i, INVALID_FORMAT)
    .required(FIELD_REQUERED),
  lastName: Yup.string().required(FIELD_REQUERED),
  username: Yup.string()
    .matches(/^[a-z]{1}[a-zA-Z0-9_.-]+$/, INVALID_FORMAT)
    .required(FIELD_REQUERED),
  email: Yup.string().email(INVALID_FORMAT).required(FIELD_REQUERED),
  password: Yup.string()
    .required(FIELD_REQUERED)
    .transform((x) => (x === "" ? undefined : x))
    .min(6, INVALID_PASSWORD_FORMAT),
  confirmPassword: Yup.string()
    .required(FIELD_REQUERED)
    .oneOf([Yup.ref("password"), null], INVALID_PASSWORD_MATCH_FORMAT),
  date: Yup.string()
    .nullable()
    .required("Please enter your age"),
});

function SignUp() {
  const formOptions = { resolver: yupResolver(validationSchema) };

  const { handleSubmit, register, control, formState } = useForm(formOptions);
  const { errors } = formState;

  const onSubmit = useCallback((values) => {
      localStorage.setItem("AUTH_TOKEN", "token");
      navigate("/");
  }, []);

  const navigate = useNavigate();

  return (
    <Row style={{ padding: "100px 0" }}>
      <Col span={10} offset={7}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Space direction="vertical" size="middle" style={{ display: "flex" }}>
            <Row gutter={16}>
              <Col span={12}>
                <Controller
                  name="firstName"
                  register={register}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormControl
                      type={"text"}
                      placeholder={"First Name"}
                      onChange={onChange}
                      value={value}
                      status={errors.firstName && STATUS_ERROR}
                    ></FormControl>
                  )}
                />
                <div className="invalid-feedback">
                  {errors.firstName?.message}
                </div>
              </Col>
              <Col span={12}>
                <Controller
                  name="lastName"
                  register={register}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormControl
                      type={"text"}
                      placeholder={"Last Name"}
                      onChange={onChange}
                      value={value}
                      status={errors.lastName && STATUS_ERROR}
                    ></FormControl>
                  )}
                />
                <div className="invalid-feedback">
                  {errors.lastName?.message}
                </div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Controller
                  name="username"
                  register={register}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormControl
                      type={"text"}
                      placeholder={"Username"}
                      onChange={onChange}
                      value={value}
                      status={errors.username && STATUS_ERROR}
                    ></FormControl>
                  )}
                />
                <div className="invalid-feedback">
                  {errors.username?.message}
                </div>
              </Col>
              <Col span={12}>
                <Controller
                  name="email"
                  register={register}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormControl
                      type={"email"}
                      placeholder={"Email"}
                      onChange={onChange}
                      value={value}
                      status={errors.email && STATUS_ERROR}
                    ></FormControl>
                  )}
                />
                <div className="invalid-feedback">{errors.email?.message}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Controller
                  name="password"
                  register={register}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormControl
                      type={"password"}
                      placeholder={"Password"}
                      onChange={onChange}
                      value={value}
                      status={errors.password && STATUS_ERROR}
                    ></FormControl>
                  )}
                />
                <div className="invalid-feedback">
                  {errors.password?.message}
                </div>
              </Col>
              <Col span={12}>
                <Controller
                  name="confirmPassword"
                  register={register}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormControl
                      type={"password"}
                      placeholder={"Confirm Password"}
                      onChange={onChange}
                      value={value}
                      status={errors.confirmPassword && STATUS_ERROR}
                    ></FormControl>
                  )}
                />
                <div className="invalid-feedback">
                  {errors.confirmPassword?.message}
                </div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Radio.Group>
                  <RadioComponent title={"Male"} value={"Male"} />
                  <RadioComponent title={"Female"} value={"Female"} />
                </Radio.Group>
              </Col>
              <Col span={12}>
                <Controller
                  name="date"
                  register={register}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <DateComponent
                      value={value}
                      status={errors.date && STATUS_ERROR}
                      onChange={onChange}
                    />
                  )}
                />
                <div className="invalid-feedback">{errors.date?.message}</div>
              </Col>
            </Row>
            <Row justify="center">
              <Col span={6}>
                <ButtonComponent buttonText={SIGN_UP} htmlType={"submit"} />
              </Col>
            </Row>
          </Space>
        </form>
      </Col>
    </Row>
  );
}

export default SignUp;
