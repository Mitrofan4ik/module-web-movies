import { configureStore } from '@reduxjs/toolkit';
import { applyMiddleware, compose } from 'redux';
import rootReducer from "../store/redusers/rootReducer";
import { middlewares } from "./middlewares";

const middlewareEnhancer = applyMiddleware(...middlewares);
const composeEnhancer = compose(middlewareEnhancer)

export const store = configureStore({
    reducer: rootReducer
})