import React from "react";
import { Radio } from "antd";

function RadioComponent({ title, value }) {
  return <Radio value={value}>{title}</Radio>;
}

export default RadioComponent;
