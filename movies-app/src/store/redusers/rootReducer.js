import { combineReducers } from 'redux';

import movieReducer from './movieSlice';
import languagesSlice from './languagesSlice';
import genresSlice from './genresSlice';

export default combineReducers({
    movies: movieReducer,
    languages: languagesSlice,
    genres: genresSlice
})