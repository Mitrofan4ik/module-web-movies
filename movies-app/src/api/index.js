import axios from 'axios';

const baseSongConfig = axios.create({
    baseURL: `https://api.themoviedb.org/3`,
    headers: {
        'Content-Type': 'application/json'
    },
});

export async function getMovies(page) {
    try {
        const { data } = await baseSongConfig.get(`/discover/movie?api_key=${process.env.REACT_APP_API_KEY}&page=${page}`);
        return data;
    } catch (error) {
        console.error("Error Reading data " + error);
    }
}

export async function getSearchMovies(searchKey, page) {
    try {
        const { data } = await baseSongConfig.get(`/search/movie?api_key=${process.env.REACT_APP_API_KEY}&query=${searchKey}&page=${page}`);
        return data;
    } catch (error) {
        console.error("Error Reading data " + error);
    }
}

export async function getGenres() {
    try {
        const { data } = await baseSongConfig.get(`/genre/movie/list?api_key=${process.env.REACT_APP_API_KEY}`);
        return data;

    } catch (error) {
        console.error("Error Reading data " + error);
    }
}

export async function getLanguages() {
    try {
        const { data } = await baseSongConfig.get(`/configuration/languages?api_key=${process.env.REACT_APP_API_KEY}`);
        return data;

    } catch (error) {
        console.error("Error Reading data " + error);
    }
}

export async function getFilteredMovies(genresId, lang, page) {
    try {
        const { data } = await baseSongConfig.get(`/discover/movie?api_key=${process.env.REACT_APP_API_KEY}&with_genres=${genresId}&with_original_language=${lang}&page=${page}`);
        return data;

    } catch (error) {
        console.error("Error Reading data " + error);
    }
}



