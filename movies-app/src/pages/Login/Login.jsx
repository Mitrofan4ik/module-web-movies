import React, { useCallback } from "react";
import { useNavigate, Link } from "react-router-dom";
import FormControl from "../../components/common/FormControl/FormControl";
import ButtonComponent from "../../components/common/Button/Button";
import { Col, Row, Space } from "antd";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {
  FIELD_REQUERED,
  INVALID_FORMAT,
  STATUS_ERROR,
  INVALID_PASSWORD_FORMAT,
  LOG_IN
} from "../../constants";

const validationSchema = Yup.object().shape({
  email: Yup.string().email(INVALID_FORMAT).required(FIELD_REQUERED),
  password: Yup.string()
    .required(FIELD_REQUERED)
    .transform((x) => (x === "" ? undefined : x))
    .min(6, INVALID_PASSWORD_FORMAT),
});

function Login() {
  const formOptions = { resolver: yupResolver(validationSchema) };

  const { handleSubmit, register, control, formState } = useForm(formOptions);
  const { errors } = formState;

  const onSubmit = useCallback((values) => {
    localStorage.setItem("AUTH_TOKEN", "token");
    navigate("/");
  }, []);

  const navigate = useNavigate();

  return (
    <Row style={{ padding: "100px 0" }}>
      <Col span={6} offset={9}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Space direction="vertical" size="middle" style={{ display: "flex" }}>
            <Row gutter={16}>
              <Col span={24}>
                <Controller
                  name="email"
                  register={register}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormControl
                      type={"email"}
                      placeholder={"Email"}
                      onChange={onChange}
                      value={value}
                      status={errors.email && STATUS_ERROR}
                    ></FormControl>
                  )}
                />
                <div className="invalid-feedback">{errors.email?.message}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={24}>
                <Controller
                  name="password"
                  register={register}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormControl
                      type={"password"}
                      placeholder={"Password"}
                      onChange={onChange}
                      value={value}
                      status={errors.password && STATUS_ERROR}
                    ></FormControl>
                  )}
                />
                <div className="invalid-feedback">
                  {errors.password?.message}
                </div>
              </Col>
            </Row>
            <Row justify="center">
              <Col span={6}>
                <ButtonComponent buttonText={LOG_IN} htmlType={"submit"} />
              </Col>
            </Row>
            <Row justify="center">
              <Col span={10}>
                <Link to={"/signup"}>Do not have an account?</Link>
              </Col>
            </Row>
          </Space>
        </form>
      </Col>
    </Row>
  );
}

export default Login;
