import { Navigate } from "react-router-dom";
import { lazy, Suspense } from "react";

const Movies = lazy(() => import("../pages/Movies/Movies"));
const Favorites = lazy(() => import("../pages/Favorites/Favorites"));
const NotFound = lazy(() => import("../pages/NotFound/NotFound"));
const Login = lazy(() => import("../pages/Login/Login"));
const SignUp = lazy(() => import("../pages/SignUp/SignUp"));
const AuthLayout = lazy(() => import("../layouts/AuthLayout"));

function getComponent(Component) {
    const TOKEN = localStorage.getItem("AUTH_TOKEN");
    return TOKEN ? (
        <Suspense>
            <Component />
        </Suspense>
    ) : (
        <Navigate to="/login" />
    );
}

export const routes = [
    {
        path: "/login",
        element: (
            <Suspense>
                <Login />
            </Suspense>
        ),
    },
    {
        path: "/signup",
        element: (
            <Suspense>
                <SignUp />
            </Suspense>
        ),
    },
    {
        path: "/",
        element:  getComponent(AuthLayout),
        children: [
            { path: "/favorites", element: getComponent(Favorites)},
            { path: "/", element: getComponent(Movies)}
        ]
    },
    {
        path: "*",
        element:  getComponent(NotFound)
    },
];
