import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";

import { Col, Row, Space, Layout, Empty } from "antd";
import MovieCard from "../../components/common/MovieCard/MovieCard";

function Favorites() {
  const { movies } = useSelector((state) => state.movies);
  const [favorite, setFavorite] = useState([]);

  const favoriteMovies = () => {
    const result = movies.filter((movie) => movie.favorite === true);
    setFavorite(result);
  };

  useEffect(() => {
    favoriteMovies();
  }, [movies]);

  return (
    <Layout.Content style={{ padding: "0 16px 16px" }}>
      <Space direction="vertical" size="middle" style={{ display: "flex" }}>
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          {!favorite.length ? (
            <Col span={12} offset={6}>
              <Empty
                description={false}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  height: "100vh",
                }}
                image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                imageStyle={{
                  height: 60,
                }}
              />
            </Col>
          ) : (
            favorite.map((movie) => (
              <Col key={movie.id} className="gutter-row" span={4}>
                <MovieCard
                  id={movie.id}
                  alt={movie.title}
                  imgSrc={movie.backdrop_path}
                  title={movie.title}
                  description={movie.overview}
                  isFavorite={movie.favorite}
                />
              </Col>
            ))
          )}
        </Row>
      </Space>
    </Layout.Content>
  );
}

export default Favorites;
