
import React,  { useEffect } from "react";
import { BrowserRouter } from "react-router-dom";
import RoutesList from "./routes/RoutesList";
import { fetchMovies } from "./store/redusers/movieSlice";
import { fetchLanguages } from "./store/redusers/languagesSlice";
import { fetchGenres } from "./store/redusers/genresSlice";
import { useDispatch, useSelector } from "react-redux";

import './App.scss';

function App() {
  const dispatch = useDispatch();
  const { page } = useSelector((state) => state.movies);

  useEffect(() => {
    dispatch(fetchMovies(page));
    dispatch(fetchGenres());
    dispatch(fetchLanguages());
  },[]);

  return (
    <BrowserRouter>
      <RoutesList />
    </BrowserRouter>
  );
}

export default App;
