import React from "react";
import { DatePicker } from "antd";

function DateComponent({ onChange, status, value }) {
  return (
    <DatePicker
      style={{ width: "100%" }}
      onChange={onChange}
      status={status}
      value={value}
    />
  );
}

export default DateComponent;
