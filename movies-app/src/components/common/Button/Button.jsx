import React from "react";
import { Button } from "antd";

function ButtonComponent({ buttonText, onClickHandler, htmlType }) {
  return (
    <Button type="primary" block onClick={onClickHandler} htmlType={htmlType}>
      {buttonText}
    </Button>
  );
}

export default ButtonComponent;
