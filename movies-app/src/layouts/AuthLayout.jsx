import React from "react";
import { Space } from "antd";
import { Outlet } from "react-router-dom";
import Header from "../components/modules/Header/Header";

function AuthLayout() {
  return (
    <Space direction="vertical" size="middle" style={{width: '100%' }}>
      <Header />
      <Outlet />
    </Space>
  );
}

export default AuthLayout;
