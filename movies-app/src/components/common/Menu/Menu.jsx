import React from "react";
import { NavLink } from "react-router-dom";
import { Menu } from "antd";
import {FAVORITES, ALL_MOVIES} from '../../../constants'

const items = [
  { label: <NavLink to="/">{ALL_MOVIES}</NavLink>, key: "all-movies" },
  { label: <NavLink to="/favorites">{FAVORITES}</NavLink>, key: "favorites" },
];

function MenuComponent() {
  return (
    <Menu items={items} mode="horizontal" defaultSelectedKeys={["all-movies"]}/>
  );
}

export default MenuComponent;
