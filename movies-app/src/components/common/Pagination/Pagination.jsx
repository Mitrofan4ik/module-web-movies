import React from "react";
import { Pagination } from "antd";

function PaginationComponent({ defaultCurrent, total, changePage }) {
  return (
    <Pagination defaultCurrent={defaultCurrent} total={total} onChange={changePage}/>
  );
}

export default PaginationComponent;
