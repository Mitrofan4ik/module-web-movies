import React, { useEffect } from "react";
import { Col, Row, Space, Layout, Empty, Card, Spin } from "antd";
import { useDispatch, useSelector } from "react-redux";
import MovieCard from "../../components/common/MovieCard/MovieCard";
import PaginationComponent from "../../components/common/Pagination/Pagination";
import SearchComponent from "../../components/common/Search/Search";
import CategoriesComponent from "../../components/common/Categories/Categories";
import {
  changePage,
  fetchSearchMovies,
  fetchFilteredMovies,
  searchValue,
} from "../../store/redusers/movieSlice";
import { addSelectedLangs } from "../../store/redusers/languagesSlice";
import { addSelectedGenres } from "../../store/redusers/genresSlice";

import { FILTER_BY_LANGUAGE, FILTER_BY_GANRES } from "../../constants";

function Movies() {
  const dispatch = useDispatch();
  const { langs, selectedLangs } = useSelector((state) => state.languages);
  const { genres, selectedGenres } = useSelector((state) => state.genres);
  const { movies, totalPages, page, search, loading } =
    useSelector((state) => state.movies);

  function changePaginationPage(value) {
    dispatch(changePage(value));
  }

  const changeFillterGenre = (filterData, checked) => {
    const nextSelectedFilter = checked
      ? [...selectedGenres, filterData.id]
      : selectedGenres.filter((t) => t !== filterData.id);
    dispatch(addSelectedGenres(nextSelectedFilter));
    dispatch(searchValue(""));
  };

  const changeFillterLang = (filterData, checked) => {
    const nextSelectedFilter = checked
      ? [...selectedLangs, filterData.iso_639_1]
      : selectedLangs.filter((t) => t !== filterData.iso_639_1);
    dispatch(addSelectedLangs(nextSelectedFilter));
    dispatch(searchValue(""));
  };

  const handleSearchMovies = (value) => {
    dispatch(addSelectedLangs([]));
    dispatch(addSelectedGenres([]));
    dispatch(searchValue(value));
  };

  useEffect(() => {
    if (search) {
      dispatch(fetchSearchMovies({ searchKey: search, page }));
    } else {
      dispatch(
        fetchFilteredMovies({
          genresId: selectedGenres,
          lang: selectedLangs,
          page,
        })
      );
    }
  }, [dispatch, page, selectedGenres, selectedLangs, search]);

  return (
    <Layout.Content style={{ padding: "0 16px 16px" }}>
      <Row>
        <Col span={6}>
          <Space
            direction="vertical"
            size="middle"
            style={{ display: "flex", paddingRight: "16px" }}
          >
            <SearchComponent onChange={handleSearchMovies} value={search} />
            <Card title={FILTER_BY_GANRES}>
              {genres.map((item) => (
                <CategoriesComponent
                  item={item}
                  key={item.id}
                  title={item.name}
                  onChangeHandler={changeFillterGenre}
                  checkedItem={item.id}
                  selectedFilter={selectedGenres}
                />
              ))}
            </Card>
            <Card title={FILTER_BY_LANGUAGE}>
              {langs.map((item) => (
                <CategoriesComponent
                  item={item}
                  key={item.iso_639_1}
                  title={item.english_name}
                  onChangeHandler={changeFillterLang}
                  checkedItem={item.iso_639_1}
                  selectedFilter={selectedLangs}
                />
              ))}
            </Card>
          </Space>
        </Col>
        <Col span={18}>
          <Space direction="vertical" size="middle" style={{ display: "flex" }}>
            {movies.length ? (
              <Row>
                <Col span={24}>
                  <PaginationComponent
                    defaultCurrent={page}
                    total={totalPages}
                    changePage={changePaginationPage}
                  />
                </Col>
              </Row>
            ) : null}
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
              {loading ? (
                <Col span={12} offset={6}>
                  <Spin
                    size="large"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      height: "100vh",
                    }}
                  />
                </Col>
              ) : !movies.length ? (
                <Col span={12} offset={6}>
                  <Empty
                    description={false}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      height: "100vh",
                    }}
                    image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                    imageStyle={{
                      height: 60,
                    }}
                  />
                </Col>
              ) : (
                movies.map((movie) => (
                  <Col key={movie.id} className="gutter-row" span={6}>
                    <MovieCard
                      id={movie.id}
                      alt={movie.title}
                      imgSrc={movie.backdrop_path}
                      title={movie.title}
                      description={movie.overview}
                      isFavorite={movie.favorite}
                    />
                  </Col>
                ))
              )}
            </Row>
          </Space>
        </Col>
      </Row>
    </Layout.Content>
  );
}

export default Movies;
