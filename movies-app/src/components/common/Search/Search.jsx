import React, {useEffect, useState, useMemo, useCallback} from "react";
import { Input } from "antd";
import {TYPE_SMTH, SEARCH} from '../../../constants';
import { debounce } from "../../../utils/debounce";

function SearchComponent({onChange, value: propsValue}) {
  const [value, setValue] = useState(propsValue)

  const onChangeHandler = useMemo(() => debounce((search) => {
    onChange(search);
  }, 500), [onChange]);

  const handleSearch = useCallback(({target}) => {
    setValue(target.value)
    onChangeHandler(target.value);
  }, [onChangeHandler]);

  useEffect(()=> {
    if(!propsValue) {
      setValue('')
    }
  }, [propsValue])

  return (
    <Input.Search
      placeholder={TYPE_SMTH}
      allowClear
      enterButton={SEARCH}
      size="large"
      value={value}
      onChange={handleSearch}
    />
  );
}

export default SearchComponent;
